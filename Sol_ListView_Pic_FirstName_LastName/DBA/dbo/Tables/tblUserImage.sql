﻿CREATE TABLE [dbo].[tblUserImage] (
    [ImageId]     NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [ImageName]   VARCHAR (50)  NULL,
    [VirtualPath] VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ImageId] ASC)
);

