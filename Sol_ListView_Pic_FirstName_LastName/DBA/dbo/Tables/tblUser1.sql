﻿CREATE TABLE [dbo].[tblUser1]
(
	[StudentId] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [FirstName] VARCHAR(50) NULL, 
    [LastName] VARCHAR(50) NULL, 
    [ImagePath] VARCHAR(MAX) NULL
)
