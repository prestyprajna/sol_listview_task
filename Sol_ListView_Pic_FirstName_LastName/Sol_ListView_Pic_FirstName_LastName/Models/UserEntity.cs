﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ListView_Pic_FirstName_LastName.Models
{
    public class UserEntity
    {
        public decimal? StudentId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ImagePath { get; set; }
    }
}