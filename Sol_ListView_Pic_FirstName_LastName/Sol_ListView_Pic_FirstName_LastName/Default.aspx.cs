﻿using Sol_ListView_Pic_FirstName_LastName.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ListView_Pic_FirstName_LastName
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        //public IQueryable<Sol_ListView_Pic_FirstName_LastName.Models.UserEntity> lvUser_GetData()
        //{
        //    return null;
        //}

        public async Task<SelectResult> lvUser_GetData(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async () =>
            {
                return new SelectResult(
                    Convert.ToInt32(await new UserDal().GetCountData()),
                    await new UserDal().GetDataPagination(startRowIndex, maximumRows)
                    );
            });
        }
    }
}