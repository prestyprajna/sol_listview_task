﻿using Sol_ListView_Pic_FirstName_LastName.DAL.ORD;
using Sol_ListView_Pic_FirstName_LastName.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ListView_Pic_FirstName_LastName.DAL
{
    public class UserDal
    {
        #region  declaration
        private UserDCDataContext _dc = null;
        #endregion

        #region  constructor
        public UserDal()
        {
            _dc = new UserDCDataContext();
        }

        #endregion

        #region  public methods

        private async Task<IEnumerable<UserEntity>> GetUserData()
        {
            return await Task.Run(() =>
            {
                var getQuery = _dc
                            ?.tblUser1s
                            ?.AsEnumerable()
                            ?.Select((leUserObj) => new UserEntity()
                            {
                                StudentId=leUserObj?.StudentId,
                                FirstName=leUserObj?.FirstName,
                                LastName=leUserObj?.LastName,
                                ImagePath=leUserObj?.ImagePath
                            })
                            ?.ToList();

                return getQuery;
                
            });
        }

        public async Task<int?> GetCountData()
        {
            return await Task.Run(async() =>
            {
                return (await this.GetUserData())
                ?.AsEnumerable()
                ?.Count();
            });
        }

        public async Task<IEnumerable<UserEntity>> GetDataPagination(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async() =>
            {
                return (await this.GetUserData())
                ?.AsEnumerable()
                ?.Skip(startRowIndex)
                ?.Take(maximumRows)
                ?.ToList();
            });
        }

        #endregion
    }
}