﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="Default.aspx.cs" Inherits="Sol_ListView_Pic_FirstName_LastName.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

   
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style  type="text/css">

        table{
            /*width:70px;
            height:70px;*/
            width:20%;
            margin:auto;
            padding:2px;
        }

        .imagePic{
            width:100px;
            height:100px;
            border-radius:100px;
            border:2px;
        }


    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <asp:ListView ID="lvUser" runat="server" DataKeyNames="StudentId" ItemType="Sol_ListView_Pic_FirstName_LastName.Models.UserEntity" SelectMethod="lvUser_GetData" >

                    <LayoutTemplate>
                        <table>

                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <asp:DataPager ID="dpStudent" runat="server" PagedControlID="lvUser" PageSize="2">
                                        <Fields>
                                            <asp:NextPreviousPagerField ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonType="Button" ButtonCssClass="w3-button w3-indigo w3-hover-green" />
                                            <asp:NumericPagerField ButtonType="Button"  NumericButtonCssClass="w3-hover-red w3-red"/>
                                            <asp:NextPreviousPagerField ShowPreviousPageButton="false" ShowNextPageButton="true" ButtonType="Button" ButtonCssClass="w3-button w3-indigo w3-hover-green" />
                                        </Fields>
                                    </asp:DataPager>
                                </td>
                            </tr>

                        </table>

                    </LayoutTemplate>

                    <ItemTemplate>

                        <tr>
                            <td>
                                <asp:Image ID="imgUser" runat="server" ImageUrl='<%#BindItem.ImagePath %>' CssClass="imagePic" />
                            </td>
                            <td>
                                <table>

                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFirstName" runat="server" Text='<%#BindItem.FirstName %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblLastName" runat="server" Text='<%#BindItem.LastName %>'></asp:Label>
                                        </td>
                                    </tr>


                                </table>

                            </td>
                        </tr>

                    </ItemTemplate>

                    <EmptyDataTemplate>
                        <span>NO RECORDS FOUND
                        </span>
                    </EmptyDataTemplate>

                </asp:ListView>

            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
